-- a. Find all artists that has letter d in its name

	SELECT * FROM artists WHERE artist_name LIKE "%d%";

-- b. Find all songs that has a length of less than 230

	SELECT * FROM songs WHERE length < 230;

-- c. Join the 'albums' and 'songs'  tables (only show the album name, song name, and song length)
	SELECT  album_title, song_title, length FROM albums JOIN songs ON albums.id = songs.album_id;


-- d. Join the 'artist' and 'albums' tables (find all albums that has letter a in its name.)
	SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id WHERE album_title LIKE "%a%";

-- e. sort the albums in Z-A order (show only the first 4 records.)
	SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- f. Join the 'albums' and 'songs' tables (sort albums from Z-A).
	SELECT * FROM albums JOIN songs ON albums.id = songs.album_id ORDER BY album_title DESC;


