-- Mini-activity: 20 mins
     --insert the following records in their respective tables
     -- Taylor Swift #1
         -- Album: Fearless 2008-11-11 #1
             --Songs: Fearless, 402, Pop Rock 
                   -- Love Story, 355, Country Pop
         
         -- Album: Red, 2012-10-22 #2
             -- Songs: State of Grace, 455, Rock, alternative rock, arena rock
                    -- Red, 341, Country
INSERT INTO artists (artist_name) VALUES ("Taylor Swift");

INSERT INTO albums (album_title, date_release, artist_id) VALUES ("Fearless", "2008-11-11", 1);
INSERT INTO albums (album_title, date_release, artist_id) VALUES ("Red", "2012-10-22", 1);

INSERT INTO songs (album_id, genre, length, song_title) VALUES (1, "Pop Rock", 402, "Fearless");
INSERT INTO songs (album_id, genre, length, song_title) VALUES (1, "Country Pop", 402, "Love Story");
INSERT INTO songs (album_id, genre, length, song_title) VALUES (2, "Alternative Rock / Arena Rock", 455, "State of Grace");
INSERT INTO songs (album_id, genre, length, song_title) VALUES (2, "Country", 341, "Red");

     -- Lady Gaga
         
         -- Album: A Star Is Born, 2018-10-05 
             -- Songs: Black Eyes, 304, Rock and roll 
                    -- Shallow, 336, Country, rock, folk rock
         
         -- Album: Born This Way, 2011-05-23 
             -- Song: Born This Way, 420, Electropop
     
INSERT INTO artists (artist_name) VALUES ("Lady Gaga");

INSERT INTO albums (album_title, date_release, artist_id) VALUES ("A Star Is Born", "2018-10-05", 2);
INSERT INTO albums (album_title, date_release, artist_id) VALUES ("Born This Way", "2011-05-23", 2);

INSERT INTO songs (album_id, genre, length, song_title) VALUES (3, "Rock and Roll", 304, "Black Eyes");
INSERT INTO songs (album_id, genre, length, song_title) VALUES (3, "Rock / Folk Rock", 336, "Shallow");
INSERT INTO songs (album_id, genre, length, song_title) VALUES (4, "Electro Pop", 420, "Born This Way");

     -- Justin Bieber
     
         -- Album: Purpose, 2015-11-13 
             -- Song: Sorry, 320, Dancehall-poptropical housemoombahton
     
         -- Album: Believe, 2012-06-15 
             -- Song: Boyfriend, 252, Pop
INSERT INTO artists (artist_name) VALUES ("Justin Bieber");

INSERT INTO albums (album_title, date_release, artist_id) VALUES ("Purposes", "2015-11-13", 3);
INSERT INTO albums (album_title, date_release, artist_id) VALUES ("Believe", "2012-06-15", 3);     

INSERT INTO songs (album_id, genre, length, song_title) VALUES (5, "Dance Hall / Pop / Tropical Housemoombahton", 320, "Sorry");
INSERT INTO songs (album_id, genre, length, song_title) VALUES (6, "Pop", 252, "Boyfriend");

     -- Ariana Grande
     
         -- Album: Dangerous Woman, 2016-05-20 
             -- Song: Into You, 405, EDM House
     
         -- Album: Thank U, Next, 2019-02-08  
             -- Song: Thank U, Next, 327, Pop, R&B
INSERT INTO artists (artist_name) VALUES ("Ariana Grande");

INSERT INTO albums (album_title, date_release, artist_id) VALUES ("Dangerous Woman", "2016-05-20", 4);
INSERT INTO albums (album_title, date_release, artist_id) VALUES ("Thank U, Next", "2019-02-08", 4);

INSERT INTO songs (album_id, genre, length, song_title) VALUES (7, "EDM House", 405, "Into You");
INSERT INTO songs (album_id, genre, length, song_title) VALUES (8, "Pop / R&B", 327, "Thank U");

     -- Bruno Mars
     
         -- Album: 24k Magic, 2016-11-18 
             -- Song: 24k Magic, 346, Funk, disco, R&B
     
         -- ALbum: Earth to Mars,2011-02-07 
             -- Song: Lost, 321, Pop
INSERT INTO artists (artist_name) VALUES ("Bruno Mars");

INSERT INTO albums (album_title, date_release, artist_id) VALUES ("24K Magic", "2016-11-18", 5);
INSERT INTO albums (album_title, date_release, artist_id) VALUES ("Earth to Mars", "2011-02-07", 5);

INSERT INTO songs (album_id, genre, length, song_title) VALUES (9, "Funk / Disco / R&B", 346, "24K Magic");
INSERT INTO songs (album_id, genre, length, song_title) VALUES (10, "Pop", 321, "Lost");




1. Exclude Records
    Terminal
    Syntax
        SELECT column_name FROM table_name WHERE column_name != value;
    Example
        SELECT * FROM songs WHERE id != 11;
        SELECT * FROM songs WHERE album_id != 8 AND album_id != 9;

2. Finding records using comparison operators
    Terminal
    Syntax
        SELECT * FROM songs WHERE length > 230;
        SELECT * FROM songs WHERE length < 200;
        SELECT * FROM songs WHERE length > 230 OR length < 200;
        SELECT * FROM songs WHERE genre = "Pop";

3. Getting records with specific conditions
    Terminal
    Syntax
        can be used for querying multiple columns
        SELECT column_name FROM table_name WHERE condition;
    Example
        SELECT * FROM songs WHERE id = 6 OR id = 7 OR id = 8;

        IN Clause
    Syntax
        SELECT column_name FROM table_name WHERE column_name IN (values);
    Example
        SELECT * FROM songs WHERE id IN (6, 7, 8);
        SELECT * FROM songs WHERE genre IN ("Pop", "Electropop", "EDM House");



4. Show records with partial match
    Terminal
        LIKE clause
        Percent(%) symbols and underscore(_) are called wildcard operators
            % -> represents zero or multiple characters
            _ -> represents single characters

        a. Find a values with a match at the start
        SELECT * FROM songs WHERE song_name LIKE "th%";

        b. Find values with a match at the end
        SELECT * FROM songs WHERE song_name LIKE "%ce";

        c. Find values with a match at any position
        SELECT * FROM songs WHERE song_name LIKE "%run%"

        d. Find values with a match of a specific length/pattern
        SELECT * FROM songs WHERE song_name LIKE "__rr_";

        e. Find values with a match at certain positions
        SELECT * FROM albums WHERE album_title LIKE "_ur%";

5. Sorting Records
    Terminal
    Syntax
        SELECT column_name FROM table_name ORDER BY column_name ORDER;
    Example
        SELECT * FROM songs ORDER BY song_name;
        SELECT * FROM songs ORDER BY song_name ASC;
        SELECT * FROM songs ORDER BY length DESC;

        -- ASC "ascending"
        -- DESC "descending"

6. Limiting Records
    Terminal
    SELECT * FROM songs LIMIT 5;

7. Showing records with distinct values
    Terminal
    SELECT genre FROM songs;
    -- using DISTINCT keyword to avoid repetition of values from retrieval
    SELECT DISTINCT genre FROM songs;

8. Joining two tables
    Terminal
    Syntax
        SELECT column_name FROM table1
        JOIN table2 ON table1.id = table2.foreign_key_column;

    Example
        SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id;

9. Joining multiple tables
    Terminal
        Syntax
        SELECT column_name FROM table1
        JOIN table2 ON table1.id = table2.foreign_key_column
        JOIN table3 ON table2.id = table3.foreign_key_column;

        Example
        SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id JOIN songs ON albums.id = songs.album_id;

10. Joining tables with specified WHERE conditions
    Example
        SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id WHERE name LIKE "%a%";

11. Selecting columns to be displayed from joining tables
    Example
        SELECT name, album_title, date_released, song_name, length, genre FROM artists JOIN albums ON artists.id = albums.artist_id JOIN songs ON albums.id = songs.album_id;

12. Providing aliases for joining table
    Terminal
    Syntax
        SELECT column_name AS alias FROM table1
        JOIN table2 ON table1.id = table2.foreign_key_column
        JOIN table3 ON table2.id = table3.foreign_key_column;

    Example
        SELECT name AS band, album_title AS album, date_released, song_name AS song, length, genre FROM artists JOIN albums ON artists.id = albums.artist_id JOIN songs ON albums.id = songs.album_id;

13. Displaying data from joining tables
    -- Create user information, a playlist and songs added to the user playlist
    INSERT INTO users (username, password, full_name, contact_number, email, address) VALUES ("thetinker", "user123", "Tine Garcia", 1123456789, "tg@mail.com", "Metro Manila");

    INSERT INTO playlists (user_id, datetime_created) VALUES (1, "2022-09-20 01:00:00");

    INSERT INTO playlists_songs (playlist_id, song_id) VALUES (1, 8), (1, 14), (1, 10);

    Joining multiple tables
    SELECT * FROM playlists JOIN playlists_songs ON playlist_id = playlists_songs.playlist_id JOIN songs ON playlists_songs.song_id = songs.id;